//frontend.js
var express = require('express');
var app = express();
var http = require('http').Server(app);
var basicAuth = require('basic-auth');

var auth = function(req, res, next) {
	function unauthorized(res) {
		res.set('WWW-Authenticate', 'Basic realm=Auth Required');
		return res.send(401);
	};

	var user = basicAuth(req);

	if (!user || !user.name || !user.pass) {
		return unauthorized(res);
	};

	if (user.name === 'override' && user.pass === 'yoloswag123') {
		return next();
	} else {
		return unauthorized(res);
	};
};


app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.get('/administer', function(req, res) {
	res.sendFile(__dirname + '/administer.html');
});

app.get('/ort', auth, function(req, res) {
	res.sendFile(__dirname + '/administer_pushtest.html');
})

app.use('/assets/images', express.static(__dirname + '/assets/images'));

app.use('/assets/css', express.static(__dirname + '/assets/css'));

http.listen(80, function() {
	console.log('listening on 80');
});
