var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function(request, response) {
	//http stuff
});

var reqobj = require('request');

server.listen(1337, function() {});

wsServer = new WebSocketServer({
	httpServer: server
});

var history = [ ];
var clients = [ ];

//ws server
wsServer.on('request', function(request) {
	var connection = request.accept(null, request.origin);

	var index = clients.push(connection) - 1;
	var username = false;
	var result = false;

	connection.on('message', function(message) {
		if(message.type === 'utf8') {
			var obj = message.utf8Data;
			var json_msg = JSON.parse(obj);
			console.log(json_msg);
			if(json_msg.client === "tester" && json_msg.rsp == "running") {
				json_retstring = JSON.stringify({action: 'none', status: 'running'});
				reqobj.post('https://api.particle.io/v1/devices/51ff73065067545734090187/start',
					{ form: { access_token: 'f8fdd9185ed764c0a43faed87b1a8d4810d0ba90'} },
					function (error, resp, body) {
						if(!error && resp.statusCode == 200) {
							console.log(body)
						}
					}
				);
				for (var i = 0; i< clients.length; i++) {
					clients[i].sendUTF(JSON.stringify({action: 'bottom', status: 'running'}));
				}			
			}
			else if (json_msg.client === "tester" && json_msg.rsp === "failed") {
				json_retstring = JSON.stringify({action: 'none', status: 'failed'});
			}
			else if (json_msg.client === "tester" &&json_msg.rsp === "finished") {
				json_retstring = JSON.stringify({action: 'finish', status: 'finished'});
			}
			else if (json_msg.client === "admin" && json_msg.rsp === "begin") {
				json_retstring = JSON.stringify({action: 'run', status: 'queued'});
			}
			else {
				json_retstring = JSON.stringify({status: 'request_error'});
			}

			for (var i = 0; i < clients.length; i++) {
				clients[i].sendUTF(json_retstring);
			}
		}
	});
	connection.on('close', function(reasonCode, description) {
		console.log('Close by ', connection.remoteAddress, ' disconnected.');
	});
});
